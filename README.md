# React Hooks

News search in React.js with hooks and axios, paginate with hooks.

## Instalation and Run the program

+ You need to have node.js installed on your computer.
+ the repository is downloaded by executing in the terminal the command, git clone https://gitlab.com/juanluisoropeza/react-news-paginated.git
+ runs in the terminal npm install, to install all the dependencies for the project to work.
+ NPM start is executed and waits for the browser to open.
