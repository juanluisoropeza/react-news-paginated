import React from 'react';
import { Col, Card } from 'react-bootstrap';
import { countAndCutText } from '../helpers';
import { INews } from './INews';

type IProps = {
  newsList: INews[] | any;
}

const News = ({ newsList }: IProps)  => {
  return (
    <>
      {newsList.length > 0 ?
        newsList.map((notice: INews) => (
          <Col xs md="4" className="mb-2 news" key={notice.url}>
            <Card>
              <div className="img-top" style={{ 'background': `url(${notice.urlToImage})` }}></div>
              <Card.Body>
                <h5>{ notice.title ? countAndCutText(notice.title, 7) : null }</h5>
                <p>{ notice.description ? countAndCutText(notice.description, 20) : 'Without content'}</p>
              </Card.Body>
            </Card>
          </Col>
        ))
      : null }
    </>
  )
};

export default News;
