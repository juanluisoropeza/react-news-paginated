import React from 'react';

type IProps = {
  newsPerPage: number;
  totalNews: number;
  currentPage: number;
  maxPageNumberLimit: number;
  minPageNumberLimit: number;
  paginate: any;
  pn: any;
}

const Pagination = ({ newsPerPage, totalNews, paginate, currentPage, maxPageNumberLimit, minPageNumberLimit, pn}: IProps) => {
  const pageNumbers: number[] = [];
  for (let i = 1; i <= Math.ceil(totalNews / newsPerPage); i++) {
    pageNumbers.push(i);
  }
  return (
    <>
      {pageNumbers.map((number) => {
        if(number < maxPageNumberLimit + 1 && number > minPageNumberLimit) {
          return (
            <li key={number} className={ `page-item ${currentPage === number ? 'active' : null }` }>
              <button type="button" className="page-link" onClick={() => {
                paginate(number);
                pn(pageNumbers);
              }}>
                {number}
              </button>
            </li>
          )
        } else {
          return null;
        }
      })}
    </>
  )
};

export default Pagination;
