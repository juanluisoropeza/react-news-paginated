export interface ISource {
  id: string;
  name: string;
}

export interface INews {
  author: string;
  content: string;
  description: string;
  publishedAt: Date;
  source: ISource;
  title: string;
  url: string;
  urlToImage: string;
}
