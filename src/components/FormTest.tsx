/* eslint-disable @typescript-eslint/no-unused-vars */
import React, { useState, useEffect, SetStateAction } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Container, Row, Col, Card, Form, Button, Alert } from "react-bootstrap";
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import News from './News';
import Pagination from './Pagination';
import { INews } from './INews';


const FormTest = () => {

  const [loading, setLoading] = useState<boolean>(false);
  const [searched, setSearched] = useState<boolean>(false);
  const [query, setQuery] = useState<string>('');
  const [news, setNews] = useState<INews[]>([]);

  const [currentPage, setCurrentPage] = useState<number>(1);
  const [newsPerPage, setNewsPerPage] = useState<number>(9);
  const [totalPages, setTotalPages] = useState<number>(0);
  const [pageNumbers, setPageNumbers] = useState<number[]>([]);

  const [pageNumberLimit, setPageNumberLimit] = useState<number>(5);
  const [maxPageNumberLimit, setMaxPageNumberLimit] = useState<number>(5);
  const [minPageNumberLimit, setMinPageNumberLimit] = useState<number>(0);

  // get current posts
  const indexOfLastPost = currentPage * newsPerPage;
  const indexOfFirstPost = indexOfLastPost - newsPerPage;

  useEffect(() => {
    if(query) {
      setNews([]);
      setSearched(true)
      setLoading(true);
      const search = async () => {
        try {
          // const url = `https://newsapi.org/v2/everything?q=${query}&pageSize=${newsPerPage}&page=${currentPage}&apiKey=e199cba2fde142d19e5e729624abb498`;
          // const url = `https://newsapi.org/v2/everything?q=${query}&pageSize=${newsPerPage}&page=${currentPage}&apiKey=de99a44f9892477cba15c619b61363f2`;
          const url = `https://newsapi.org/v2/everything?q=${query}&pageSize=${newsPerPage}&page=${currentPage}&apiKey=413c531bae344070bbfb01005c409ea5`;
          const result = await axios.get(url);
          setTotalPages(result.data.totalResults);
          setNews(result.data.articles);
          setLoading(false);
        } catch (error) {
          setLoading(false);
          console.log(error.response.data);
        }
      };
      search();
    }
  }, [query, currentPage, newsPerPage]);

  const formik = useFormik({
    initialValues: {
      search: '',
    },
    validationSchema: Yup.object({
      search: Yup.string().required('Search is required.'),
    }),
    onSubmit: (formData) => {
      setQuery(formData.search);
      formik.resetForm();
    }
  });

  // change page
  const paginate = (pageNumber: React.SetStateAction<number>) => {
    setCurrentPage(pageNumber);
    setQuery(query)
  };

  const handlePageNumbers = (pageNumbers: React.SetStateAction<number[]>) => {
    setPageNumbers(pageNumbers);
  };

  const handleNext = () => {
    setCurrentPage(currentPage + 1);
    if(currentPage + 1 > maxPageNumberLimit) {
      setMaxPageNumberLimit(maxPageNumberLimit + pageNumberLimit);
      setMinPageNumberLimit(minPageNumberLimit + pageNumberLimit);
    }
  };

  const handlePrev = () => {
    setCurrentPage(currentPage - 1);
    if ((currentPage - 1) % pageNumberLimit === 0) {
      setMaxPageNumberLimit(maxPageNumberLimit - pageNumberLimit);
      setMinPageNumberLimit(minPageNumberLimit - pageNumberLimit);
    }
  };

  let pageIncrementBtn = null;
  if(pageNumbers.length > maxPageNumberLimit) {
    pageIncrementBtn = <li className="page-item" onClick={handleNext}>
      <button
        type="button"
        className="page-link"
      >
        &hellip;
      </button>
    </li>;
  }

  let pageDecrementBtn = null;
  if (minPageNumberLimit >= 1) {
    pageDecrementBtn = <li className="page-item" onClick={handlePrev}>
      <button
        type="button"
        className="page-link"
      >
        &hellip;
      </button>
    </li>;
  }

  return (
    <Container>
      <Row className="justify-content-md-center my-3">
        <Col xs md="6">
          <Card>
            <Card.Body>
              <Form onSubmit={formik.handleSubmit}>
                <Form.Row className="align-items-start">
                  <Col xs="10">
                    <Form.Control
                      type="text"
                      placeholder="Search"
                      name="search"
                      autoFocus
                      onChange={formik.handleChange}
                      isInvalid={!!formik.errors.search}
                      value={formik.values.search}
                    />
                    {
                      formik.touched.search && formik.errors.search && (
                        <Form.Control.Feedback type="invalid">
                          {formik.errors.search}
                        </Form.Control.Feedback>
                      )
                    }
                  </Col>
                  <Col xs="2">
                    <Button type="submit" variant="success">
                      Search
                    </Button>
                  </Col>
                </Form.Row>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      { loading ? (
        <Row className="justify-content-md-center my-3">
          <FontAwesomeIcon className="spinner" icon={faSpinner} />
        </Row>
        ) : null
      }
      <Row className="justify-content-md-start my-3">
        { searched && !loading && news.length === 0 ? (
          <Col md="12">
            <Row className="justify-content-md-center">
              <Col md="6">
                <Alert variant="danger">
                  No hay resultados.
                </Alert>
              </Col>
            </Row>
          </Col>
        ) : (
          <News
            newsList={news}
          />
        )}
      </Row>
      { !loading ? (
      <Row>
        <Col className="my-3">
          { totalPages > 0 ? (
            <ul className="justify-content-end pagination">
              <li className={`page-item ${currentPage === pageNumbers.length + 1 ? 'disabled' : null}`}>
                <button
                  type="button"
                  className="page-link"
                  disabled={ currentPage === pageNumbers.length + 1 ? true : false }
                  onClick={handlePrev}
                >
                  &laquo;
                </button>
              </li>
              { pageDecrementBtn }
              <Pagination
                newsPerPage={newsPerPage}
                currentPage={currentPage}
                totalNews={totalPages}
                maxPageNumberLimit={maxPageNumberLimit}
                minPageNumberLimit={minPageNumberLimit}
                paginate={paginate}
                pn={handlePageNumbers}
              />
              { pageIncrementBtn }
              <li className={`page-item ${currentPage === pageNumbers[pageNumbers.length - 1] ? 'disabled' : null}`}>
                <button
                  type="button"
                  className="page-link"
                  disabled={ currentPage === pageNumbers[pageNumbers.length - 1] ? true : false }
                  onClick={handleNext}
                >
                  &raquo;
                </button>
              </li>
            </ul>
          ) : null }
        </Col>
      </Row>
      ) : null}
    </Container>
  )
};

export default FormTest;
