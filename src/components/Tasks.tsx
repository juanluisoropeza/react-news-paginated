import React, { useState, useRef } from "react";
import { Container, Row, Col, Card, Form, Button } from "react-bootstrap";

type FormElement = React.FormEvent<HTMLFormElement>;
interface ITask {
  name: string;
  done: boolean;
}

const Tasks = () => {
  const [newTask, setNewTask] = useState<string>("");
  const [tasks, setTasks] = useState<ITask[]>([]);
  const taskInput = useRef<HTMLInputElement>(null);

  const handleSubmit = (e: FormElement): void => {
    e.preventDefault();
    addTask(newTask);
    setNewTask("");
    taskInput.current?.focus();
  };

  const addTask = (name: string): void => {
    const newTasks: ITask[] = [...tasks, { name, done: false }];
    setTasks(newTasks);
  };

  const toggleDoneTask = (i: number): void => {
    const actualTasks: ITask[] = [...tasks];
    actualTasks[i].done = !actualTasks[i].done;
    setTasks(actualTasks);
  };

  const removeTask = (i: number) => {
    const actualTasks: ITask[] = [...tasks];
    actualTasks.splice(i, 1);
    setTasks(actualTasks);
  };

  return (
    <Container>
      <Row className="justify-content-md-center">
        <Col xs md="4">
          <Card>
            <Card.Body>
              <Form onSubmit={handleSubmit}>
                <Form.Row className="align-items-center">
                  <Col xs="auto">
                    <Form.Control
                      type="text"
                      placeholder="New Task"
                      onChange={(e) => setNewTask(e.target.value)}
                      value={newTask}
                      autoFocus
                      ref={taskInput}
                    />
                  </Col>
                  <Col xs="auto">
                    <Button type="submit" variant="success">
                      Save
                    </Button>
                  </Col>
                </Form.Row>
              </Form>
            </Card.Body>
          </Card>
        </Col>
      </Row>
      <Row className="justify-content-md-center">
        {tasks.map((task: ITask, i: number) => (
          <Col xs md="4" key={i} className="mt-2">
            <Card>
              <Card.Body>
                <h2 style={{ textDecoration: task.done ? "line-through" : "" }}>
                  {task.name}
                </h2>
                <div>
                  <Button size="sm" onClick={() => toggleDoneTask(i)}>
                    {task.done ? "✓" : "✗"}
                  </Button>
                  <Button
                    size="sm"
                    variant="danger"
                    onClick={() => removeTask(i)}
                  >
                    Delete Task
                  </Button>
                </div>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default Tasks;
