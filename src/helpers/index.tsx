export const countAndCutText = (str: string, cant: number) => {
  str = str.replace(/(^\s*)|(\s*$)/gi,"");
  str = str.replace(/[ ]{2,}/gi," ");
  str = str.replace(/\n /,"\n");
  const strLength = str.split(' ').length;
  let finalContent = '';
  for (let i = 0; i < str.split(' ').length; i++) {
    const element = str.split(' ')[i];
    if(i < cant) {
      finalContent += element;
      if(strLength !== Math.floor(i - 1)) {
        finalContent += ' ';
      }
    }
  }
  if(strLength > cant) {
    finalContent += '...';
  }
  return finalContent;
}
