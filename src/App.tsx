import React from 'react';
import FormTest from './components/FormTest';
import { Container, Row, Col } from "react-bootstrap";

const App = () => {
  return (
    <>
      <Container>
        <Row className="my-4">
          <Col className="text-center">
            <h1>Test For Vindow (React.js | Hooks)</h1>
            <h3>Juan Luis Oropeza</h3>
          </Col>
        </Row>
      </Container>
      <FormTest />
    </>
  );
};

export default App;
